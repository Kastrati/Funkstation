﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Core.Models
{
    public class InfoModel
    {
        public string Username { get; set; }

        public string MachineName { get; set; }

        public string Gpu { get; set; }

        public string Cpu { get; set; }

        public string Mac{ get; set; }

        public string Board { get; set; }

        public string Bios { get; set; }

        public string Ram { get; set; }

        public string Os { get; set; }
    }
}
