﻿using System;
using System.Management;

namespace Client.Core.Helpers {
    internal class HardwareInfo {

        public static string GetProcessorId() {
            var mc = new ManagementClass("win32_processor");
            var moc = mc.GetInstances();
            var Id = string.Empty;
            foreach (ManagementObject mo in moc) {
                Id = mo.Properties["processorID"].Value.ToString();
                break;
            }
            return Id;
        }
        
        /// <summary>
        ///     Retrieving System MAC Address.
        /// </summary>
        /// <returns></returns>
        public static string GetMacAddress() {
            var mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
            var moc = mc.GetInstances();
            var MACAddress = string.Empty;
            foreach (ManagementObject mo in moc) {
                if (MACAddress == string.Empty) {
                    if ((bool) mo["IPEnabled"])
                        MACAddress = mo["MacAddress"].ToString();
                }

                mo.Dispose();
            }

            MACAddress = MACAddress.Replace(":", "");
            return MACAddress;
        }

        /// <summary>
        ///     Retrieving Motherboard Manufacturer.
        /// </summary>
        /// <returns></returns>
        public static string GetBoardMaker() {
            var searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_BaseBoard");

            foreach (ManagementObject wmi in searcher.Get()) {
                try {
                    return wmi.GetPropertyValue("Manufacturer").ToString();
                }

                catch {
                }
            }

            return "Board Maker: Unknown";
        }

        /// <summary>
        ///     Retrieving BIOS Maker.
        /// </summary>
        /// <returns></returns>
        public static string GetBIOSmaker() {
            var searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_BIOS");

            foreach (ManagementObject wmi in searcher.Get()) {
                try {
                    return wmi.GetPropertyValue("Manufacturer").ToString();
                }

                catch {
                }
            }

            return "BIOS Maker: Unknown";
        }

        /// <summary>
        ///     Retrieving Physical Ram Memory.
        /// </summary>
        /// <returns></returns>
        public static string GetPhysicalMemory() {
            var oMs = new ManagementScope();
            var oQuery = new ObjectQuery("SELECT Capacity FROM Win32_PhysicalMemory");
            var oSearcher = new ManagementObjectSearcher(oMs, oQuery);
            var oCollection = oSearcher.Get();

            long MemSize = 0;
            long mCap = 0;

            // In case more than one Memory sticks are installed
            foreach (ManagementObject obj in oCollection) {
                mCap = Convert.ToInt64(obj["Capacity"]);
                MemSize += mCap;
            }

            MemSize = MemSize / 1024 / 1024;
            return MemSize + "MB";
        }


        //Get CPU Temprature.
        /// <summary>
        ///     method for retrieving the CPU Manufacturer
        ///     using the WMI class
        /// </summary>
        /// <returns>CPU Manufacturer</returns>
        public static string GetCPUManufacturer() {
            var cpuMan = string.Empty;
            //create an instance of the Managemnet class with the
            //Win32_Processor class
            var mgmt = new ManagementClass("Win32_Processor");
            //create a ManagementObjectCollection to loop through
            var objCol = mgmt.GetInstances();
            //start our loop for all processors found
            foreach (ManagementObject obj in objCol) {
                if (cpuMan == string.Empty) {
                    // only return manufacturer from first CPU
                    cpuMan = obj.Properties["Manufacturer"].Value.ToString();
                }
            }

            return cpuMan;
        }

        /// <summary>
        ///     Retrieving Current Language.
        /// </summary>
        /// <returns></returns>
        public static string GetOSInformation() {
            var searcher = new ManagementObjectSearcher("SELECT * FROM Win32_OperatingSystem");
            foreach (ManagementObject wmi in searcher.Get()) {
                try {
                    return ((string) wmi["Caption"]).Trim() + ", " + (string) wmi["Version"] + ", " +
                        (string) wmi["OSArchitecture"];
                }
                catch {
                }
            }

            return "BIOS Maker: Unknown";
        }

        /// <summary>
        ///     Retrieving Processor Information.
        /// </summary>
        /// <returns></returns>
        public static string GetProcessorInformation() {
            var mc = new ManagementClass("win32_processor");
            var moc = mc.GetInstances();
            var info = string.Empty;
            foreach (ManagementObject mo in moc) {
                var name = (string) mo["Name"];
                name = name.Replace("(TM)", "™").Replace("(tm)", "™").Replace("(R)", "®").Replace("(r)", "®")
                           .Replace("(C)", "©").Replace("(c)", "©").Replace("    ", " ").Replace("  ", " ");

                info = name + ", " + (string) mo["Caption"] + ", " + (string) mo["SocketDesignation"];
                //mo.Properties["Name"].Value.ToString();
                //break;
            }

            return info;
        }

        /// <summary>
        ///     Retrieving Computer Name.
        /// </summary>
        /// <returns></returns>
        public static string GetComputerName() {
            var mc = new ManagementClass("Win32_ComputerSystem");
            var moc = mc.GetInstances();
            var info = string.Empty;
            foreach (ManagementObject mo in moc) {
                info = (string) mo["Name"];
                //mo.Properties["Name"].Value.ToString();
                //break;
            }

            return info;
        }

        public static string GetGpu() {
            
            var searcher  = new ManagementObjectSearcher("SELECT * FROM Win32_DisplayConfiguration");
            var graphicsCard = string.Empty;

            foreach (var o in searcher.Get()) {
                var mo = (ManagementObject) o;
                foreach (var property in mo.Properties)
                {
                    if (property.Name == "Description")
                    {
                        return property.Value.ToString();
                    }
                }
            }

            return graphicsCard;
        }
    }
}
