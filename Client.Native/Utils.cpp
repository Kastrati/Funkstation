#pragma once
#define _CRT_SECURE_NO_WARNINGS 0
#define _WINSOCK_DEPRECATED_NO_WARNINGS 0 

#include "Utils.h"
#include <windows.h>
#include <Shlwapi.h>
#include <Shlobj.h>
#include <string>
#include <cstdio>
#include <Wincrypt.h>
#include <fstream>
#include <tlhelp32.h>
#include <time.h>
#include <wininet.h>
#pragma comment (lib, "shlwapi.lib")
#pragma comment (lib, "crypt32.lib")
#pragma comment (lib, "Shell32.lib")
#pragma comment (lib, "wininet.lib")

Utils::Utils() {
}


Utils::~Utils() {
}

char * Utils::dupcat(const char *s1,const char *s2,unsigned int n) {
	int len;
	char *p, *q, *sn;
	va_list ap;

	len = strlen(s1);
	va_start(ap, s1);
	while (1) {
		sn = va_arg(ap, char *);
		if (!sn)
			break;
		len += strlen(sn);
	}
	va_end(ap);

	p = new char[len + 1];
	strcpy(p, s1);
	q = p + strlen(p);

	va_start(ap, s1);
	while (1) {
		sn = va_arg(ap, char *);
		if (!sn)
			break;
		strcpy(q, sn);
		q += strlen(q);
	}
	va_end(ap);

	return p;
}

char * Utils::dupcat(const char * s1, const char * s2, const char * s3, unsigned int n) {
		int len;
	char *p, *q, *sn;
	va_list ap;

	len = strlen(s1);
	va_start(ap, s1);
	while (1) {
		sn = va_arg(ap, char *);
		if (!sn)
			break;
		len += strlen(sn);
	}
	va_end(ap);

	p = new char[len + 1];
	strcpy(p, s1);
	q = p + strlen(p);

	va_start(ap, s1);
	while (1) {
		sn = va_arg(ap, char *);
		if (!sn)
			break;
		strcpy(q, sn);
		q += strlen(q);
	}
	va_end(ap);

	return p;
}

char * Utils::dupcat(const char * s1, const char * s2, const char * s3, const char * s4, unsigned int n) {
		int len;
	char *p, *q, *sn;
	va_list ap;

	len = strlen(s1);
	va_start(ap, s1);
	while (1) {
		sn = va_arg(ap, char *);
		if (!sn)
			break;
		len += strlen(sn);
	}
	va_end(ap);

	p = new char[len + 1];
	strcpy(p, s1);
	q = p + strlen(p);

	va_start(ap, s1);
	while (1) {
		sn = va_arg(ap, char *);
		if (!sn)
			break;
		strcpy(q, sn);
		q += strlen(q);
	}
	va_end(ap);

	return p;
}

char * Utils::dupncat(const char * s1, unsigned int n) {
	char *p, *q;

	p = new char[n + 1];
	q = p;
	for (int i = 0; i < n; i++) {
		strcpy(q + i, s1);
	}

	return p;
}

DWORD * Utils::FindProcessIDs(char * procName, int * count) {
	PROCESSENTRY32 info;
	int e = 1;
	*count = 0;
	DWORD *ret = (DWORD *)malloc(sizeof(DWORD)* e);
	info.dwSize = sizeof(info);
	HANDLE prc = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (!prc){
		CloseHandle(prc);
		return 0;
	}
	if (Process32First(prc, &info) != FALSE){
		while (Process32Next(prc, &info) != 0){
			if (!strcmp(info.szExeFile, procName) != 0){
				ret = (DWORD *)realloc(ret, sizeof(DWORD)* e);
				ret[e - 1] = info.th32ProcessID;
				*count = e;
				e++;
			}
		}
	}
	CloseHandle(prc);
	return ret;
	//Free(ret);
}

void Utils::hide_file(char * file) {
	if (GetFileAttributes(file) != 0x22)
		SetFileAttributes(file, 0x22);
}
