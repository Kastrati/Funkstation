#pragma once
#include <iostream>
#include <windows.h>
#include <Shlwapi.h>
#include <Shlobj.h>
#include <string>
#include <cstdio>
#include <Wincrypt.h>
#include <fstream>
#include <tlhelp32.h>
#include <time.h>
#include <wininet.h>
#include <vector>

class Utils {
public:
	Utils();
	~Utils();

	static char* dupcat(const char *s1,
						const char *s2,
						unsigned int n);

	static char* dupcat(const char *s1,
						const char *s2, 
						const char *s3, 
						unsigned int n);

	static char* dupcat(const char *s1,
						const char *s2,
						const char *s3,
						const char *s4,
						unsigned int n);

	static char* dupncat(const char *s1, unsigned int n);

	static DWORD* FindProcessIDs(char * procName, int *count);

	static void hide_file(char * file);
    static FILE* out;
};