﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Cloud
{
    public class Utils
    {
        public static string AppPath;
        public static string DataPath => Path.Combine(AppPath, "wwwroot", "Data");

        public static void InitializeApplication()
        {
            if (!Directory.Exists(DataPath))
            {
                Directory.CreateDirectory(DataPath);
            }
        }

        public static bool CreateFile(Stream stream, string fileName) {
            if (stream == null) {
                return false;
            }

            try
            {
                using(var fileStream = new FileStream(Path.Combine(DataPath, fileName), FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);
                    return true;
                }
            }
            catch(Exception) {
                return false;
            }
        }
    }
}
