﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Cloud.Controller
{
    [Route("api/[controller]/[action]")]
    public class FunkstationController : Microsoft.AspNetCore.Mvc.Controller
    {
        [HttpPost]
        [ActionName("uploadFunk")]
        [RequestSizeLimit(1500_0000_000)]
        public IActionResult UploadFunk(IFormFile file)
        {
            if (file == null) {
                return BadRequest();
            }
            var stream = file.OpenReadStream();
            var name = file.FileName;
            var status = Utils.CreateFile(stream, name);
            if (status) {
                return Ok();
            }

            return BadRequest();
        }
    }
}
